<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AideEController extends Controller
{
    /**
     * @Route("/aideE", name="aideE")
     */
    public function index()
    {
        return $this->render('aideE/aideE.html.twig', [
            'controller_name' => 'AideEController',
        ]);
    }
}
