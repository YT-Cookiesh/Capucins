<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ListEntrepriseEController extends Controller
{
    /**
     * @Route("/listEntrepriseE", name="listEntrepriseE")
     */
    public function index()
    {
        return $this->render('listEntrepriseE/listEntrepriseE.html.twig', [
            'controller_name' => 'ListEntrepriseEController',
        ]);
    }
}
