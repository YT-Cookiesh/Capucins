<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class EffectifController extends Controller
{
    /**
     * @Route("/effectif", name="effectif")
     */
    public function index()
    {
        return $this->render('effectif/effectif.html.twig', [
            'controller_name' => 'EffectifController',
        ]);
    }
}
