<?php

namespace App\Controller;

use App\Entity\Userprof;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;

class ListProfesseurController extends Controller
{
    /**
     * @Route("/listProfesseur", name="listProfesseur")
     */
    public function index()
    {
        $professeur = $this->getDoctrine()
            ->getRepository(Userprof::class)
            ->findAll();
        return $this->render('listProfesseur/listProfesseur.html.twig', compact('professeur'));

    }

    /**
     * @Route("/listProfesseur/ajout", name="ajoutProfesseur")
     */
    public function ajoutEleve(Request $request)
    {
        $item = new Userprof();

        $item->setNomProf('');
        $item->setPrenomProf('');
        $item->setLogin('');
        $item->setPassword('');
        $item->setRole('admin');

        $form = $this->createFormBuilder($item)
            ->add('nomProf', TextType::class, array(
                'label' => 'Nom :')
            )
            ->add('prenomProf', TextType::class, array(
                'label' => 'Prenom :'
            ))
            ->add('login', TextType::class, array(
                'label' => 'Identifiant :'
            ))
            ->add('password', PasswordType::class, array(
                'label' => 'Mot de passe :'
            ))
            ->add('role', HiddenType::class, array(
                'label' => 'Rôle :'
            ))
            ->add('present',  ChoiceType::class, array(
                'choices'  => array(
                    'Oui' => true,
                    'Non' => false,
                ),
                'label' => 'Présent :'
            ))
            ->getForm();

        // Par défaut, le formulaire renvoie une demande POST au même contrôleur qui la restitue.
        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));
            if ($form->isSubmitted() && $form->isValid()) {
                $item = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($item);
                $em->flush();
                return $this->redirectToRoute('listProfesseur');
            }
        }

        return $this->render('listProfesseur/ajout.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/listProfesseur/supprimer/{id}", name="supprimerProfesseur")
     */
    public function supprimerProfesseur($id)
    {
        $item = $this->getDoctrine()
            ->getRepository(Userprof::class)
            ->find($id);

        if (!$item) {
            throw $this->createNotFoundException(
                "Aucun professeur n'a été trouvée via l'id " . $id
            );
        }else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($item);
            $em->flush();
        }

        // Par défaut on retourne à la liste
        return $this->redirectToRoute('listProfesseur');
    }

    /**
     * @Route("/listProfesseur/modifier/{id}", name="modifierProfesseur")
     */
    public function modifierProfesseur(Request $request, $id)
    {
        $item = $this->getDoctrine()
            ->getRepository(Userprof::class)
            ->find($id);
        if (!$item) {
            throw $this->createNotFoundException(
                "Aucun professeur n'a été trouvée via l'id " . $id
            );
        } else {
            $form = $this->createFormBuilder($item)
                ->add('nomProf', TextType::class)
                ->add('prenomProf', TextType::class)
                ->add('login', TextType::class)
                ->add('password', PasswordType::class)
                ->add('present',  ChoiceType::class, array(
                    'choices'  => array(
                        'Oui' => true,
                        'Non' => false,
                    ),
                ))
                ->getForm();
        }
        // Par défaut, demande POST au même contrôleur qui la restitue.
        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));
            if ($form->isSubmitted() && $form->isValid()) {
                $item = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($item);
                $em->flush();
                return $this->redirectToRoute('listProfesseur');
            }
        }
        return $this->render('listProfesseur/ajout.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
