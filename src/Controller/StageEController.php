<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class StageEController extends Controller
{
    /**
     * @Route("/stageE", name="stageE")
     */
    public function index()
    {
        return $this->render('stageE/stageE.html.twig', [
            'controller_name' => 'StageEController',
        ]);
    }
}
